{
  "Cps-Version": "0.8.1",
  "Name": "spdlog",
  "Configuration": "release",
  "Components": {
    "spdlog": {
      "Location": "@prefix@/lib/libspdlog.1.12.0.dylib",
      "Link-Name": "@rpath/libspdlog.1.12.dylib"
    }
  }
}
