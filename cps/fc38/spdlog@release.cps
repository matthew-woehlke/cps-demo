{
  "Cps-Version": "0.8.1",
  "Name": "spdlog",
  "Configuration": "release",
  "Components": {
    "spdlog": {
      "Location": "@prefix@/lib64/libspdlog.so.1.11.0",
      "Link-Name": "libspdlog.so.1.11"
    }
  }
}
