{
  "Cps-Version": "0.8.1",
  "Name": "spdlog",
  "Version": "1.11.0",
  "Compat-Version": "1.0.0",
  "Platform": {
    "Isa": "x86_64",
    "Kernel": "linux",
    "C-Runtime-Vendor": "gnu",
    "C-Runtime-Version": 2.37
  },
  "Requires": {
    "fmt": {
      "Version": 9
    }
  },
  "Default-Components": [
    "spdlog"
  ],
  "Configurations": [
    "release",
    "debug"
  ],
  "Components": {
    "spdlog": {
      "Type": "dylib",
      "Requires": [
        "fmt:fmt"
      ],
      "Compile-Features": [
        "threads"
      ],
      "Definitions": [
        "SPDLOG_SHARED_LIB",
        "SPDLOG_COMPILED_LIB",
        "SPDLOG_FMT_EXTERNAL"
      ],
      "Includes": [
        "@prefix@/include"
      ]
    },
    "spdlog-header-only": {
      "Type": "interface",
      "Requires": [
        "fmt:fmt"
      ],
      "Compile-Features": [
        "threads"
      ],
      "Definitions": [
        "SPDLOG_FMT_EXTERNAL"
      ],
      "Includes": [
        "@prefix@/include"
      ]
    }
  }
}
