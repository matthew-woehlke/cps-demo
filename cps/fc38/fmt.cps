{
  "Cps-Version": "0.8.1",
  "Name": "fmt",
  "Version": "9.1.0",
  "Compat-Version": "9.0.0",
  "Platform": {
    "Isa": "x86_64",
    "Kernel": "linux",
    "C-Runtime-Vendor": "gnu",
    "C-Runtime-Version": 2.37
  },
  "Default-Components": [
    "fmt"
  ],
  "Configurations": [
    "release",
    "debug"
  ],
  "Components": {
    "fmt": {
      "Type": "dylib",
      "Compile-Features": [
        "c++11"
      ],
      "Definitions": [
        "FMT_SHARED"
      ],
      "Includes": [
        "@prefix@/include"
      ]
    },
    "fmt-header-only": {
      "Type": "interface",
      "Compile-Features": [
        "c++11"
      ],
      "Definitions": [
        "FMT_HEADER_ONLY=1"
      ],
      "Includes": [
        "@prefix@/include"
      ]
    }
  }
}
