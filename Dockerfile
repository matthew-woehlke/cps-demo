FROM fedora:38

# Install necessary dependencies to build CMake
RUN dnf install -y \
    openssl-devel \
    gcc-c++ \
    git \
    ninja-build

# Fetch CMake
RUN git clone \
    --depth 1 \
    --branch cps-cppcon-demo-2023 \
    https://gitlab.kitware.com/matthew-woehlke/cmake.git \
    /cmake

# Build (bootstrap) CMake
RUN cd /cmake && ./bootstrap --generator=Ninja
RUN cd /cmake && ninja

# Install necessary dependencies for demo
RUN dnf install -y \
    fmt-devel \
    spdlog-devel

# Add demo files
ADD CMakeLists.txt /demo/
ADD demo.cpp /demo/
ADD cps/fc38/* /demo/cps/fc38/

# Build demo
RUN cd /demo && \
    mkdir build && \
    cd build && \
    /cmake/bin/cmake -G Ninja .. && \
    ninja && \
    ./demo
